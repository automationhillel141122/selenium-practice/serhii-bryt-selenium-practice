package org.brit;

import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.io.FileUtils;
import org.brit.driver.WebDriverFactory;
import org.brit.driver.WebdriverHolder;
import org.brit.driver.WebdriverType;
import org.brit.listeners.MyTestListener;
import org.brit.utils.ConfigProperties;
import org.brit.utils.MyFileUtils;
import org.brit.utils.PropertyReader;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

@Listeners(MyTestListener.class)
public class BaseTestClass {
    protected SeleniumLib seleniumLib;
    protected ConfigProperties cfg = ConfigFactory.create(ConfigProperties.class);

    @BeforeSuite(alwaysRun = true)
    public void beforeSuite() {
        MyFileUtils.createDownloadDirectory();
        MyFileUtils.createScreenshotsDirectory();
        seleniumLib = new SeleniumLib();
    }

    @AfterSuite
    public void afterSuite() {
        if (WebdriverHolder.getInstance().getDriver() != null) {
            WebdriverHolder.getInstance().getDriver().quit();
        }
    }

//    @AfterMethod
//    public void makeScreenshotIfNeeded(ITestResult testResult){
//        if (!testResult.isSuccess()){
//            File screenshotAs = ((TakesScreenshot) WebdriverHolder.getInstance().getDriver())
//                    .getScreenshotAs(OutputType.FILE);
//            File screenshot = new File(MyFileUtils.DirectoryFor.SCREENSHOTS.getDirName(),
//                    testResult.getName() + new Date().getTime() + ".png");
//            try {
//                FileUtils.copyFile(screenshotAs, screenshot);
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }
//        }
//    }

}
