package org.brit;

public class StringsWork {

    public static void main(String[] args) {

        String s1 = "Hello ";
        String s2 = "world ";
        String s3 = "!!!";

        String s4 = s1 + s2 + s3;

        System.out.println(s4);

        System.out.println("Hello " + s2 + " !!!");

        String print = "%s %s %s".formatted("Hello ", s2, s3);

        System.out.println(print);

        System.out.printf("%s %s %s\n", "Hello ", s2, s3);

        printString(s2);

    }

    public static void printString(String string){
        System.out.println("%s '%s' %s".formatted("Hello ", string, " !!!!"));
    }



}
