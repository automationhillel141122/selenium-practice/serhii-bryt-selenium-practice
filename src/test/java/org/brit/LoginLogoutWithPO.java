package org.brit;

import org.brit.pages.login_logout.LoginPage;
import org.brit.pages.login_logout.SecureAreaPage;
import org.brit.utils.PropertyReader;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginLogoutWithPO extends BaseTestClass {
    private String userName = cfg.userName();//PropertyReader.getProperty("user.name");
    private String pass = cfg.userPassword();//PropertyReader.getProperty("user.password");

    LoginPage loginPage;

    @BeforeClass
    public void beforeThisClass() {
        seleniumLib.goToUrl(/*PropertyReader.getProperty("base.url")*/ cfg.baseUrl() + "/login");
        loginPage = new LoginPage();
    }

    @Test
    public void loginSuccess() {
        loginPage = new LoginPage();
        SecureAreaPage secureAreaPage = loginPage.successfulLogin(userName, pass);
        Assert.assertTrue(secureAreaPage.isSuccessMessageVisible());

        loginPage = secureAreaPage.logout();
        Assert.assertTrue(loginPage.getSuccessMessageText().contains("Youlogged out of the secure area!"),
                "Success message does not contain text: 'Youlogged out of the secure area!'");
    }

    @Test(dataProvider = "getData")
    public void loginUnSuccessLogin(String userName, String pass, String message) {
        loginPage = loginPage
                .unSuccessfulLogin(userName, pass);
        String errorMessageText = loginPage.getErrorMessageText();
        Assert.assertTrue(errorMessageText.contains(message), "Error message does not contain text: '" + message + "'");
        Assert.assertEquals(loginPage.getHeaderText(), "Login Page");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {userName + "1", pass, "Your username is invalid!"},
                {userName, pass + "1", "Your password is invalid!"},
                {"", pass, "Your username is invalid!"},
                {"", "", "Your username is invalid!"},
                {userName, "", "Your password is invalid!"}
        };
    }
}
