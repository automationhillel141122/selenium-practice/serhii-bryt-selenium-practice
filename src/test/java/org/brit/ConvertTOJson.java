package org.brit;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.Data;
import lombok.SneakyThrows;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConvertTOJson {

    public static void main(String[] args) {
        User user = new User();
        user.setFirstNameShouldBeIncluded(false);
        user.setFirstName(null);
        GsonBuilder gsonBuilder = new Gson().newBuilder().serializeNulls();
        String s = gsonBuilder.create().toJson(user);
        System.out.println(s);
    }

    @Data
    @JsonAdapter(CustomTypeAdapter.class)
    static class User {
        private List<String> fieldNames = new ArrayList<>();
        @Expose
        @SerializedName("firstName")
        private String firstName = null;
        @Expose
        @SerializedName("lastName")
        private String lastName;
        @Expose
        @SerializedName("age")
        private Integer age;
        @Expose
        @SerializedName("retired")
        private Boolean retired;


        private boolean firstNameShouldBeIncluded = true;
        private boolean lastNameShouldBeIncluded = true;
        private boolean ageShouldBeIncluded = true;
        private boolean retiredShouldBeIncluded = true;



        public User() {
            Faker faker = new Faker();
            firstName = faker.name().firstName();
            lastName = faker.name().lastName();
            age = faker.number().numberBetween(25, 90);
            retired = age > 65;
        }

        public void setFieldsNames(String... fieldsNames){
            if (fieldsNames.length == 0){
                return;
            } else {
                Collections.addAll(this.fieldNames, fieldsNames);
            }
        }
    }

    static class CustomTypeAdapter extends TypeAdapter<User> {
        @SneakyThrows
        @Override
        public void write(JsonWriter jsonWriter, User user) throws IOException {
            jsonWriter.beginObject();
            for (Field field : user.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Expose.class)) {
                    if (field.getAnnotation(Expose.class).serialize()) {
                        if (user.getClass().getDeclaredField(field.getName() + "ShouldBeIncluded").getBoolean(user)) {
                            if (field.isAnnotationPresent(SerializedName.class)) {
                                JsonWriter name = jsonWriter.name(field.getAnnotation(SerializedName.class).value());
                                if (field.getType().equals(Integer.class))
                                      name.value((Integer)field.get(user));
                                else if (field.getType().equals(Double.class))
                                    name.value((Double) field.get(user));
                                else if (field.getType().equals(Boolean.class))
                                    name.value((Boolean) field.get(user));
                                else name.value((String) field.get(user));
                            }
                        }
                    }
                }
            }
            jsonWriter.endObject();
        }

        @Override
        public User read(JsonReader jsonReader) throws IOException {
            return null;
        }
    }
}
