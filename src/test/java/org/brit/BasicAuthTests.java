package org.brit;

import org.brit.driver.WebdriverHolder;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BasicAuthTests extends BaseTestClass{
    @Test
    public void basicAuthTest(){
        seleniumLib.goToUrl("http://admin:admin@the-internet.herokuapp.com/basic_auth");
        Assert.assertEquals(WebdriverHolder.getInstance().getDriver().findElement(By.xpath("//h3")).getText(), "Basic Auth");

    }
}
