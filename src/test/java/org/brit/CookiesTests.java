package org.brit;

import org.brit.driver.WebdriverHolder;
import org.brit.pages.login_logout.LoginPage;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Set;

public class CookiesTests extends BaseTestClass{
    @Test
    public void cookiesTest(){
        seleniumLib.goToUrl(cfg.baseUrl() + "/login");
        new LoginPage()
                .successfulLogin(cfg.userName(), cfg.userPassword());
        WebDriver driver = WebdriverHolder.getInstance().getDriver();
        Set<Cookie> cookies = driver.manage().getCookies();
        cookies.forEach(System.out::println);

        Cookie cookie = new Cookie("MyCookie", "CookieValue");

        driver.manage().addCookie(cookie);

        cookies = driver.manage().getCookies();

        Assert.assertTrue(cookies.contains(cookie));

        driver.manage().deleteCookie(cookie);

        cookies = driver.manage().getCookies();

        Assert.assertFalse(cookies.contains(cookie));

        driver.manage().deleteAllCookies();
        System.out.println();

        LocalStorage localStorage = ((WebStorage)driver).getLocalStorage();

        localStorage.setItem("MyLocalStarageItem", "ItemValue");

        System.out.println();

    }
}
