package org.brit;

import org.brit.driver.WebdriverHolder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ActionsTests extends BaseTestClass {
    @Test
    public void dnd1() {
        seleniumLib.goToUrl("https://www.globalsqa.com/demoSite/practice/droppable/photo-manager.html");

        By elementBy = By.xpath(".//li/h5[text()='High Tatras']");

        WebElement gallery = WebdriverHolder.getInstance().getDriver().findElement(By.id("gallery"));
        WebElement element = gallery.findElement(elementBy);
        WebElement trash = WebdriverHolder.getInstance().getDriver().findElement(By.id("trash"));

        Actions actions = new Actions(WebdriverHolder.getInstance().getDriver());

        actions
                .dragAndDrop(element, trash)
                .pause(Duration.ofSeconds(5))
                .build()
                .perform();
        List<WebElement> elements = trash.findElements(elementBy);
        Assert.assertEquals(elements.size(), 1);

        actions
                .moveToElement(trash.findElement(elementBy)
                        .findElement(By.xpath("./../img")))
                .pause(Duration.ofSeconds(3))
                .clickAndHold()
                .pause(Duration.ofSeconds(3))
                .moveToElement(gallery)
                .pause(Duration.ofSeconds(3))
                .release()
                .pause(Duration.ofSeconds(3))
                .build()
                .perform();

        elements = gallery.findElements(elementBy);

        Assert.assertEquals(elements.size(), 1);
    }

    @BeforeClass
    public void beforeClass() {
        seleniumLib.goToUrl("http://the-internet.herokuapp.com/hovers");
    }

    @Test(dataProvider = "dataProvider")
    public void hoverTest(int index) {
        WebElement element = WebdriverHolder.getInstance().getDriver()
                .findElement(By.xpath("//div[@class='figure'][%s]".formatted(index)));
        new Actions(WebdriverHolder.getInstance().getDriver())
                .moveToElement(element)
                .build()
                .perform();
        Assert.assertEquals(element.findElement(By.xpath(".//h5")).getText(),
                "name: user" + index);
    }

    @DataProvider
    public Object[] dataProvider() {
        return new Object[]{1, 2, 3};
    }
}
