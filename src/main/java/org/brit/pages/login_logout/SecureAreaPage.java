package org.brit.pages.login_logout;

import io.qameta.allure.Step;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class SecureAreaPage extends BaseLoginLogoutPage{

    @FindBy(css = "a.button")
    private WebElement logoutButton;

    public SecureAreaPage() {
        super();
    }

    @Step("Logout...")
    public LoginPage logout(){
        log.info("Log out");
        logoutButton.click();
        return new LoginPage();
    }

}
