package org.brit.pages.login_logout;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.brit.driver.WebdriverHolder;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

@Slf4j
public class BaseLoginLogoutPage {

    @FindBy(css = ".example h2")
    private WebElement pageHeader;

    @FindBy(css = "#flash.success")
    private WebElement successMessage;

    @FindBy(css = "#flash.error")
    private WebElement errorMessage;


    public BaseLoginLogoutPage() {
        PageFactory.initElements(WebdriverHolder.getInstance().getDriver(), this);
    }

    @Step("Getting header text")
    public String getHeaderText() {
        log.info("Getting header text");
        return pageHeader.getText();
    }

    @Step("Check if success message is visible")
    public boolean isSuccessMessageVisible(){
        return successMessage.isDisplayed();
    }


    @Step("Getting success message")
    public String getSuccessMessageText(){
        log.info("Getting success message text");
        return successMessage.getText();
    }

    @Step("Check if error message is visible")
    public boolean isErrorMessageVisible(){
        return errorMessage.isDisplayed();
    }

    @Step("Getting error message")
    public String getErrorMessageText(){
        log.info("Getting error message text");
        return errorMessage.getText();
    }
}
