package org.brit.pages.login_logout;

import io.qameta.allure.Step;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Slf4j
public class LoginPage extends BaseLoginLogoutPage {

    @FindBy(id = "username")
    private WebElement nameInputField;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInputField;

    @FindBy(css = "button.radius")
    private WebElement loginButton;

    public LoginPage() {
        super();
    }


    @Step("Enter User name: {userName}")
    private LoginPage enterUserName(String userName) {
        nameInputField.sendKeys(userName);
        return this;
    }

    @Step("Enter User password: {password}")
    private LoginPage enterUserPassword(String password) {
        passwordInputField.sendKeys(password);
        return this;
    }

    @Step("Click login button")
    private void clickLoginButton() {
        loginButton.click();
    }


    @Step("Make successful login with: {userName}")
    public SecureAreaPage successfulLogin(String userName, String userPassword) {
        log.info("Logging with userName: " + userName + " password: " + userPassword);
        enterUserName(userName)
                .enterUserPassword(userPassword)
                .clickLoginButton();
        return new SecureAreaPage();
    }

    @Step("Make unsuccessful login with: {userName}")
    public LoginPage unSuccessfulLogin(String userName, String userPassword) {
        log.info("Logging with userName: " + userName + " password: " + userPassword);
        enterUserName(userName)
                .enterUserPassword(userPassword)
                .clickLoginButton();
        return new LoginPage();
    }


}
