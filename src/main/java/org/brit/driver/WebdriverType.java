package org.brit.driver;

public enum WebdriverType {
    CHROME,
    FIREFOX,
    SAFARI
}
